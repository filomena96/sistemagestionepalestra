package controls;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.io.ObjectInputStream.GetField;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import entities.Bilancio;


public class GestoreBilancio
{
	private float totale_entrate = 0;
	private float totale_uscite = 0;
	private float bilancio_complessivo = 0;
	
	public GestoreBilancio()
	{}
	
	public GestoreBilancio(String dataInizio, String dataFine)
	{
		try {
			calcolaBilancio(dataInizio, dataFine);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void calcolaBilancio (String dataInizio, String dataFine ) throws ParseException {

		List<Bilancio> lista_entrate = leggiEntrate(dataInizio, dataFine);
		List<Bilancio> lista_uscite = leggiUscite(dataInizio, dataFine);
		
		for(int i=0; i<lista_entrate.size(); i++)
			this.totale_entrate += lista_entrate.get(i).getImporto();
		
		for(int i=0; i<lista_uscite.size(); i++)
			this.totale_uscite += lista_uscite.get(i).getImporto();
		
		this.bilancio_complessivo = totale_entrate - totale_uscite;
		
	}
	
	public static void registraPagamento(String tipo, Bilancio bilancio)
	{
		if (tipo=="ENTRATA")
			Bilancio.registraEntrata(bilancio);
		else if (tipo=="USCITA")
			Bilancio.registraUscita(bilancio);
	}
	
	public static List<Bilancio> leggiEntrate(String dataInizio, String dataFine) throws ParseException
	{
		Date dataI=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataInizio);
		Date dataF=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataFine);
		
		List<Bilancio> lista_entrate = new ArrayList<Bilancio>();
		
		for(int i=0; i<Bilancio.leggiEntrate().size(); i++)
		{
			Date data = (Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (Bilancio.leggiEntrate().get(i).getData());
			if ( data.compareTo(dataI) >0 && data.compareTo(dataF)<0)
				lista_entrate.add(Bilancio.leggiEntrate().get(i));
		}
		
		return lista_entrate;
	}
	
	public static List<Bilancio> leggiUscite(String dataInizio, String dataFine) throws ParseException
	{
		
		Date dataI=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataInizio);
		Date dataF=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataFine);
		
		List<Bilancio> lista_uscite = new ArrayList<Bilancio>();
		
		for(int i=0; i<Bilancio.leggiUscite().size(); i++)
		{
			Date data = (Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (Bilancio.leggiUscite().get(i).getData());
			if ( data.compareTo(dataI) >0 && data.compareTo(dataF)<0)
				lista_uscite.add(Bilancio.leggiUscite().get(i));
		}
		
		return lista_uscite;
	}
	
	public float getTotaleEntrate()
	{
		return totale_entrate;
	}
	
	public float getTotaleUscite()
	{
		return totale_uscite;
	}
	
	public float getBilancioComplessivo()
	{
		return bilancio_complessivo;
	}

}



