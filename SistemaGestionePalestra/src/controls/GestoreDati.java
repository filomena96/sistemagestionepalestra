package controls;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Bilancio;
import entities.Cliente;

public class GestoreDati
{
	private List<Cliente> lista_clienti;
	
	public GestoreDati()
	{
		this.lista_clienti = Cliente.leggiClienti();
	}
	

	
	
	public ArrayList<String> leggiStringaDatiClienti()
	{
		ArrayList<String> lista = new ArrayList<String>();
		
		for(int i=0; i<lista_clienti.size(); i++)
			lista.add(i, (i+1)+") "+
					   lista_clienti.get(i).getNome()+" "+
					   lista_clienti.get(i).getCognome()+" - "+
					   lista_clienti.get(i).getDataDiNascita()+" - "+
					   lista_clienti.get(i).getCodiceFiscale());
		
		return lista;
	}
	
	public List<Cliente> getListaClienti()
	{
		return lista_clienti;	
	}
	

}
