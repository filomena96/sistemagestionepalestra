package entities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Serializable ci permette di salvare un oggetto su uno stream di byte
//e successivamente su file

public class Bilancio implements Serializable
{	

    static final long serialVersionUID = 1L;  //dichiarazione versione univoca della classe 

	private static final String FILE_ENTRATE = "entrate.ser";
	private static final String FILE_USCITE = "uscite.ser";

	private static boolean entrateInizializzate = false;
	private static boolean usciteInizializzate = false;
	
	private static List<Bilancio> lista_entrate = new ArrayList<Bilancio>();
	private static List<Bilancio> lista_uscite = new ArrayList<Bilancio>();

	private String codice_fiscale = null;
	private String causale = null;
	private String data = null;
	private float importo = 0;
	
	
	public Bilancio()
	{}



	// Aggiunge una "entrata" alla lista del bilancio ed aggiorna il db
	public static void registraEntrata(Bilancio bilancio)
	{
		if (!entrateInizializzate)
			lista_entrate = loadList(FILE_ENTRATE);



		lista_entrate.add(bilancio);

		writeList(lista_entrate,FILE_ENTRATE);
	}



	// Aggiunge una "uscita" alla lista del bilancio ed aggiorna il db
	public static void registraUscita(Bilancio bilancio)
	{
		if (!usciteInizializzate)
			lista_uscite = loadList(FILE_USCITE);



		lista_uscite.add(bilancio);

		writeList(lista_uscite, FILE_USCITE);
	}



	// Restituisce la lista aggiornata del bilancio
	public static List<Bilancio> leggiEntrate()
	{
		if (!entrateInizializzate)
			lista_entrate = loadList(FILE_ENTRATE);

		return  lista_entrate;
	}

	public static List<Bilancio> leggiUscite()
	{
		if (!usciteInizializzate)
			lista_uscite = loadList(FILE_USCITE);
		
		return lista_uscite;
	}



	/* ---- Metodi private --------------------------------------------------------*/


	// Funzione che scrive su file la lista di oggetti "Bilancio"
	private static void writeList(List <Bilancio> lista, String filename)
	{
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(lista);
			oos.close();


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	// Funzione che carica da file la lista di oggetti "Bilancio"
	private static List <Bilancio> loadList(String filename)
	{
		List <Bilancio> lista = new ArrayList<Bilancio>();;
		lista.clear();

		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));)
		{
			lista = (List<Bilancio>) ois.readObject();
			ois.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			// Se il file non esiste lo crea
			if(filename==FILE_ENTRATE)
				writeList(lista_entrate, filename);
			else if (filename==FILE_USCITE)
				writeList(lista_uscite, filename);
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		if (filename==FILE_ENTRATE)
		entrateInizializzate = true;
		else if (filename==FILE_USCITE)
		usciteInizializzate = true;
		
		return lista;
	}




	/* ---- Metodi GET ------------------------------------------------------------*/


	public String getCodiceFiscale()
	{
		return codice_fiscale;
	}




	public float getImporto()
	{
		return importo;
	}





	public String getCausale()
	{
		return causale;
	}




	public String getData()
	{
		return data;
	}




	/* ---- Metodi SET ------------------------------------------------------------*/


	public void setCodiceFiscale(String codice_fiscale)
	{
		this.codice_fiscale = codice_fiscale;
	}




	public void setImporto(float importo)
	{
		this.importo = importo;
	}







	public void setCausale(String causale)
	{
		this.causale = causale;
	}




	public void setData(String data)
	{
		this.data = data;
	}



}
