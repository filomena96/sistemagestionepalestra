package entities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Cliente implements Serializable
{
    static final long serialVersionUID = 1L;
    
	private static final String FILE_CLIENTI = "clienti.ser";
	
	private static boolean clientiInizializzati = false;
	
	private static List<Cliente> lista_clienti = new ArrayList<Cliente>();

	private String nome = "";
	private String cognome = "";
	private String data_di_nascita = "";
	private String codice_fiscale = "";
	private String scadenza_abbonamento = "";
	

	/* ----- Costruttori ----- */
	
	public Cliente()
	{}
	
	public Cliente(String nome, String cognome, String data_di_nascita, String codice_fiscale)
	{
		this.nome = nome;
		this.cognome = cognome;
		this.setData_di_nascita(data_di_nascita);
		this.setCodice_fiscale(codice_fiscale);
	}
	
	
	
	/* ----- Metodi pubblici ----- */
	
	// Aggiunge un "cliente" alla lista dei clienti ed aggiorna il db
	public static void registraCliente(Cliente cliente)
	{
		if (!clientiInizializzati)
			lista_clienti = loadList(FILE_CLIENTI);

		lista_clienti.add(cliente);

		writeList(lista_clienti, FILE_CLIENTI);
	}
	
	
	// Restituisce la lista aggiornata dei clienti
	public static List<Cliente> leggiClienti()
	{
		if (!clientiInizializzati)
			lista_clienti = loadList(FILE_CLIENTI);

		return  lista_clienti;
	}
	
	
	
	
	/* ----- Metodi privati ----- */
	
	// Funzione che scrive su file la lista di oggetti "Bilancio"
	private static void writeList(List <Cliente> lista, String filename)
	{
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(lista);
			oos.close();


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	// Funzione che carica da file la lista di oggetti "Cliente"
	private static List <Cliente> loadList(String filename)
	{
		List <Cliente> lista = new ArrayList<Cliente>();;
		lista.clear();

		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));)
		{
			lista = (List<Cliente>) ois.readObject();
			ois.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			// Se il file non esiste lo crea
			writeList(lista_clienti, filename);
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 

		clientiInizializzati = true;
		
		return lista;
	}

	
	
	
	/* ----- Metodi SET ----- */
	
	
	public void setNome(String nome)
	{
		this.nome = nome;
	}
	

	public void setCognome(String cognome)
	{
		this.cognome = cognome;
	}
	

	public void setData_di_nascita(String data_di_nascita) {
		this.data_di_nascita = data_di_nascita;
	}

	
	public void setCodice_fiscale(String codice_fiscale) {
		this.codice_fiscale = codice_fiscale;
	}
	
	
	public void setScadenzaAbbonamento(String scadenza_abbonamento) {
		this.scadenza_abbonamento = scadenza_abbonamento;
	}
	
	
	
	
	/* ----- Metodi GET ----- */
	
	
	public String getNome()
	{
		return nome;
	}
	
	
	public String getCognome()
	{
		return cognome;
	}
	
	
	public String getDataDiNascita() {
		return data_di_nascita;
	}
	
	
	public String getCodiceFiscale() {
		return codice_fiscale;
	}
	
	
	public String getScadenzaAbbonamento() {
		return scadenza_abbonamento;
	}
	

	
	
}