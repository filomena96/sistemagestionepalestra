package boundaries;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import controls.GestoreBilancio;
import controls.GestoreDati;
import entities.Bilancio;

import javax.swing.SpinnerModel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;

public class PaginaPagamento extends JFrame
{
	private boolean dataValid = false;
	
	private static final String[] Mesi = { "GENNAIO", "FEBBRAIO", "MARZO", "APRILE", "MAGGIO", "GIUGNO",
			"LUGLIO", "AGOSTO", "SETTEMBRE", "OTTOBRE", "NOVEMBRE", "DICEMBRE"};
	
	private JTextField txtFieldImporto; 
	private JTextField txtFieldCausale;
	
	private GestoreDati gestoreDati;
	
	public PaginaPagamento()
	{
		gestoreDati = new GestoreDati();
		
		getContentPane().setLayout(null); // metodo che recupera il container in ogni jframe
		setBounds(200, 200, 800, 600); // fissa la posizione e le dimensioni di una finestra nell'interfaccia grafica
		
		JLabel lblTitle = new JLabel("REGISTRA PAGAMENTO");
		lblTitle.setBackground(Color.LIGHT_GRAY);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(10, 11, 764, 40);
		getContentPane().add(lblTitle);
		
		
		JPanel panel_IntervalloDate = new JPanel();
		panel_IntervalloDate.setBounds(27, 93, 698, 383);
		getContentPane().add(panel_IntervalloDate);
		panel_IntervalloDate.setLayout(null);

		JSpinner spinGiorno = new JSpinner();
		spinGiorno.setBounds(98, 29, 43, 22);
		panel_IntervalloDate.add(spinGiorno);
		spinGiorno.setModel(new SpinnerNumberModel(1, 1, 31, 1));

		JLabel lblGiorno = new JLabel("Giorno");
		lblGiorno.setHorizontalAlignment(SwingConstants.CENTER);
		lblGiorno.setBounds(98, 11, 43, 16);
		panel_IntervalloDate.add(lblGiorno);


		JSpinner spinMese = new JSpinner(new SpinnerListModel(Mesi));
		spinMese.setBounds(151, 29, 81, 22);
		panel_IntervalloDate.add(spinMese);


		JLabel lblMese = new JLabel("Mese");
		lblMese.setHorizontalAlignment(SwingConstants.CENTER);
		lblMese.setBounds(151, 11, 81, 16);
		panel_IntervalloDate.add(lblMese);

		JSpinner spinAnno = new JSpinner();
		spinAnno.setBounds(242, 29, 63, 22);
		panel_IntervalloDate.add(spinAnno);
		spinAnno.setModel(new SpinnerNumberModel(2020, 2000, 2050, 1));

		JLabel lblAnno = new JLabel("Anno");
		lblAnno.setHorizontalAlignment(SwingConstants.CENTER);
		lblAnno.setBounds(242, 11, 63, 16);
		panel_IntervalloDate.add(lblAnno);
		
		JLabel lblData = new JLabel("Data:");
		lblData.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblData.setBounds(23, 27, 46, 21);
		panel_IntervalloDate.add(lblData);
		
		JLabel lblTipo = new JLabel("Tipo:");
		lblTipo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTipo.setBounds(23, 66, 46, 22);
		panel_IntervalloDate.add(lblTipo);
		
		JSpinner spinTipo = new JSpinner();
		spinTipo.setModel(new SpinnerListModel(new String[] {"ENTRATA", "USCITA"}));
		spinTipo.setBounds(98, 70, 81, 22);
		panel_IntervalloDate.add(spinTipo);

		
		JLabel lblImporto = new JLabel("Importo:");
		lblImporto.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblImporto.setBounds(23, 141, 109, 22);
		panel_IntervalloDate.add(lblImporto);
		
		txtFieldImporto = new JTextField();
		txtFieldImporto.setBounds(98, 145, 86, 20);
		panel_IntervalloDate.add(txtFieldImporto);
		txtFieldImporto.setColumns(1);
		
		JLabel lblCausale = new JLabel("Causale: ");
		lblCausale.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCausale.setBounds(23, 194, 109, 22);
		panel_IntervalloDate.add(lblCausale);
		
		txtFieldCausale = new JTextField();
		txtFieldCausale.setColumns(1);
		txtFieldCausale.setBounds(98, 198, 234, 20);
		panel_IntervalloDate.add(txtFieldCausale);
		
		JLabel lblCliente = new JLabel("Cliente:");
		lblCliente.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblCliente.setBounds(23, 250, 109, 22);
		panel_IntervalloDate.add(lblCliente);
		
		JSpinner spinCliente = new JSpinner();
		spinCliente.setBounds(98, 254, 334, 22);
		
		ArrayList<String> lista_clienti = new ArrayList<String>();
		lista_clienti.add("NESSUNO");
		lista_clienti.addAll(gestoreDati.leggiStringaDatiClienti());
		spinCliente.setModel(new SpinnerListModel(lista_clienti.toArray()));
		panel_IntervalloDate.add(spinCliente);
		
		JButton btnConferma = new JButton("Conferma");
		btnConferma.setBounds(543, 333, 145, 39);
		panel_IntervalloDate.add(btnConferma);
		
		JLabel lblErrore = new JLabel("");
		lblErrore.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblErrore.setHorizontalAlignment(SwingConstants.CENTER);
		lblErrore.setBounds(23, 345, 453, 14);
		panel_IntervalloDate.add(lblErrore);
		
		JButton btnAnnulla = new JButton("Annulla");
		btnAnnulla.setBounds(438, 333, 86, 39);
		panel_IntervalloDate.add(btnAnnulla);
		
		btnAnnulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				
			}
		});

		
		
		spinTipo.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e)
			{

				if(spinTipo.getValue().equals("ENTRATA"))
				{
					lblCliente.setVisible(true);
					spinCliente.setVisible(true);
				}
				else if (spinTipo.getValue().equals("USCITA"))
				{
					lblCliente.setVisible(false);
					spinCliente.setVisible(false);
				}
			}
		});
		
		btnAnnulla.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
				SistemaGestionePalestra sistemaGestionePalestra = new SistemaGestionePalestra();
				sistemaGestionePalestra.setVisible(true);	
			}
		});
		
		
		btnConferma.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				
				Bilancio bilancio = new Bilancio();
				
				String gg=spinGiorno.getValue().toString();
				String mm=monthToNum(spinMese.getValue().toString());
				String aa=spinAnno.getValue().toString();
				
				bilancio.setData(gg +"/" +mm + "/" + aa);
				
				bilancio.setCausale(txtFieldCausale.getText());
				
				try
				{
					float importo = Float.parseFloat(txtFieldImporto.getText());
					bilancio.setImporto(importo);
					dataValid = true;					

				} catch (Exception e2)
				{
					dataValid = false;
					lblErrore.setText("ERRORE: importo non valido!");
				}
				
				if(dataValid)
				{
					if(spinTipo.getValue().equals("ENTRATA"))
					{
						bilancio.setCodiceFiscale(spinCliente.getName());
						
						if(!spinCliente.getValue().equals("NESSUNO"))
						{
							String[] stringaCliente = spinCliente.getValue().toString().split("\\)");						
							int indexCliente = Integer.parseInt(stringaCliente[0])-1;
							bilancio.setCodiceFiscale(gestoreDati.getListaClienti().get(indexCliente).getCodiceFiscale());
						}
						
						GestoreBilancio.registraPagamento("ENTRATA", bilancio);
						
						
					}
					else if(spinTipo.getValue().equals("USCITA"))
					{
						GestoreBilancio.registraPagamento("USCITA", bilancio);
					}
					
					JOptionPane.showMessageDialog(getContentPane(),
						    "Pagamento registrato!");
					dispose();
					SistemaGestionePalestra sistemaGestionePalestra = new SistemaGestionePalestra();
					sistemaGestionePalestra.setVisible(true);
				}
				
				
				
				
				
			}
		});
		
		
	}
	
	private String monthToNum(String str_mese)
	{



		if(str_mese == Mesi[0])
			return "01";
		else if(str_mese == Mesi[1])
			return "02";
		else if(str_mese == Mesi[2])
			return "03";
		else if(str_mese == Mesi[3])
			return "04";
		else if(str_mese == Mesi[4])
			return "05";
		else if(str_mese == Mesi[5])
			return "06";
		else if(str_mese == Mesi[6])
			return "07";
		else if(str_mese == Mesi[7])
			return "08"; 
		else if(str_mese == Mesi[8])
			return "09";
		else if(str_mese == Mesi[9])
			return "10";
		else if(str_mese == Mesi[10])
			return "11";
		else
			return "12";
	}
}
