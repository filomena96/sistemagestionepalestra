package boundaries;
import java.io.*;
import java.net.*;

public class Client
{
	
    Socket miosocket = null;
	int porta = 6789;  //porta server
	DataOutputStream out;
	DataInputStream in;
	BufferedReader tastiera;
	String messaggio;
	
	public void Comunica ()
	{
		try 
		{
			do
			{
			
			tastiera = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("[2] - Messaggio da inviare al server: ");
		     messaggio = tastiera.readLine();  //con readline andiamo a leggere quello che scriviamo da tastiera
			System.out.println("[3] - Invio: " + messaggio);
			out.writeBytes(messaggio + "\n");
			System.out.println("[4] - In attesa di una risposta...");
			String ricevuta = in.readLine();
			System.out.println("[5] - Risposta del server: " + ricevuta);
			
		} while (!messaggio.toLowerCase().equals("esci"));
		}
		catch (IOException e) 
		{
			
			e.printStackTrace();
		}
		
		
		
	}
	
	public Socket connetti()              //dovr� permetterci di collegarci al server
	{
		try
		{
			System.out.println("[0] - Provo a connettermi al server...");
			Socket miosocket = new Socket (InetAddress.getLocalHost(),porta);  //ci connettiamo al socket
		                                          //registrando il canale nella variabile server
		
			System.out.println("[1] - Connesso!");
		    in = new DataInputStream(miosocket.getInputStream());
		    out = new DataOutputStream(miosocket.getOutputStream());

		} catch (UnknownHostException e) {
			
			System.err.println("Host sconosciuto");
			
		} catch (Exception e)
		
		{
			
			System.err.println("Impossibile stabilire la connessione");
		}
		
		return miosocket;
	}
	
	
	
	public static void main (String[] args) {
		
		Client c = new Client ();
		c.connetti();
		c.Comunica();
		
	}

}
