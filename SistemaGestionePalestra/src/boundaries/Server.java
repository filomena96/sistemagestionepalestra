package boundaries;

import java.io.*;
import java.net.*;

public class Server  {

	private static final long serialVersionUID = 1L;
	//dichiariamo un oggetto di tipo socket e lo inizializziamo

	ServerSocket server = null;
	Socket socketClient = null;

	//porta del server

	int porta = 6789;  //va bene qualsiasi numero l'importante che sia superiore a 1023

	//dobbiamo simulare i due impulsi I/O (Richiesta e risposta) e quindi dichiariamo due oggetti

	DataOutputStream out;
	DataInputStream in;
	String letto;

	/*legge quello che scrive il client
	 * lo riscrive in maiuscolo
	 * e lo ricomunica al client
	 */
	public void Comunica ()
	{		
		try
		{
			do
			{
				System.out.println("[3] - Aspetto un messaggio dal client...");
				letto = in.readLine();  //leggiamo dallo stream di input
				System.out.println("[4] - Messaggio ricevuto: " + letto);
				String risposta = letto.toUpperCase();  // letto in maiuscolo
				System.out.println("[5] - Rispondo con: " + risposta);
				out.writeBytes(risposta + "\n");  //scriviamo sullo stream di output
			} while(!letto.toLowerCase().equals("esci"));
			System.out.println("[6] - Chiudo la connessione.");
			socketClient.close();  //chiudiamo la connessione con il client

		}


		catch (IOException e)
		{

			e.printStackTrace();
		}  
	}

	//creiamo il metodo per la connessione

	public Socket attendi()
	{

		try {

			System.out.println("[0] - Inizializzo il Server...");


			//inizializziamo il servizio
			server = new ServerSocket(porta);
			System.out.println("[1] - Server pronto in ascolto sulla porta: " + porta);

			//mi metto in ascolto sulla porta che ho aperto
			socketClient = server.accept();

			System.out.println("[2] - Connessione stabilita con un client!");

			//evitiamo connessioni multiple
			server.close();

			//dobbiamo specificare dove andiamo a scrivere e dove andiamo a leggere

			//flusso di input

			in = new DataInputStream(socketClient.getInputStream());

			//flusso di output

			out = new DataOutputStream(socketClient.getOutputStream());

		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return socketClient;

	}


	public static void main (String[] args)
	{

		Server s = new Server();
		s.attendi();
		s.Comunica();

	}
}