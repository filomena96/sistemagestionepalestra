package boundaries;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;

import entities.Bilancio;
import entities.Cliente;

import java.awt.Color;


/**
 * La classe Sistema_Gestione_Palestra realizza l'interfaccia principale dell'app
 * consente all'utilizzatore di selezionare la funzionalitą di suo interesse
 * @author filom
 *
 */
public class SistemaGestionePalestra extends JFrame
{
	private static PaginaBilancio framePaginaBilancio; 
	private static SistemaGestionePalestra frameHome; 
	private static PaginaPagamento framePaginaPagamento;

	public SistemaGestionePalestra() {
		getContentPane().setLayout(null); // metodo che recupera il container in ogni jframe
		setBounds(200, 200, 800, 600); // fissa la posizione e le dimensioni di una finestra nell'interfaccia grafica

		JLabel lblTitle = new JLabel("SISTEMA DI GESTIONE PALESTRA");
		lblTitle.setBackground(Color.LIGHT_GRAY);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(10, 11, 764, 40);
		getContentPane().add(lblTitle);

		JButton btnRegistraCliente = new JButton("Registra nuovo cliente");
		btnRegistraCliente.setBounds(290, 106, 200, 40);
		getContentPane().add(btnRegistraCliente);

		JButton btnVisualizzaBilancio = new JButton("Visualizza bilancio");
		// registra il btnVisualizzaBilancio stesso come
		// ascoltatore degli eventi
		btnVisualizzaBilancio.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				dispose();
				framePaginaBilancio.setVisible(true); //mostra Pagina bilancio

			}
		});
		btnVisualizzaBilancio.setBounds(290, 157, 200, 40);
		getContentPane().add(btnVisualizzaBilancio); //add aggiunge pannello jpanel
		
		JButton brnRegistraPagamento = new JButton("Registra pagamento");
		brnRegistraPagamento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				framePaginaPagamento.setVisible(true);
			}
		});
		brnRegistraPagamento.setBounds(290, 208, 200, 40);
		getContentPane().add(brnRegistraPagamento);
	}




	/**
	 * --------------- MAIN ------------------------------------------------------------------
	 * Il main implementa la navigazione attraverso le varie interfacce grafiche (libreria swing)
	 * 
	 */
	public static void main (String[] args)
	{
		/*Bilancio entrate;
		Bilancio uscite;
		for (int i=1; i<10; i++)
		{
			entrate = new Bilancio();
			entrate.setData("0"+i+"/06/2020");
			entrate.setCausale("causale entr n."+i);
			entrate.setImporto(60);
			Bilancio.registraEntrata(entrate);
			
			uscite = new Bilancio();
			uscite.setData("0"+i+"/06/2020");
			uscite.setCausale("causale usc n."+i);
			uscite.setImporto(45);
			Bilancio.registraUscita(uscite);
		}*/
		
		

		/*Cliente cliente;
		for(int i=1; i<10; i++)
		{
			cliente = new Cliente();
			cliente.setNome("nome"+i);
			cliente.setCognome("cognome"+i);
			cliente.setData_di_nascita(i+"/02/1993");
			cliente.setCodice_fiscale("codicefiscale"+i);
			Cliente.registraCliente(cliente);
		}*/
		
		
		
		
		
		
		
		
        //Lancia un evento (Runnable) alla fine dell'elenco degli eventi Swing
		//viene elaborato dopo l'elaborazione di tutti gli eventi della GUI precedenti.
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try 
				{

					framePaginaBilancio = new PaginaBilancio(); 
					frameHome = new SistemaGestionePalestra();
					framePaginaPagamento = new PaginaPagamento();

					framePaginaBilancio.setVisible(false);
					framePaginaPagamento.setVisible(false);
					frameHome.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	} //Fine del main
}
