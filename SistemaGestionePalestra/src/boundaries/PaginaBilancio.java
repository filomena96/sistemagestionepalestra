package boundaries;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;

import controls.GestoreBilancio;
import entities.Bilancio;

import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerListModel;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpinnerModel;
import javax.swing.JTable;

public class PaginaBilancio extends JFrame {

/**
 *Pagina Bilancio � una boundary che implementa l'interfaccia grafica relativa alla visualizzazione del bilancio 
 */

	private static final String[] Mesi = { "GENNAIO", "FEBBRAIO", "MARZO", "APRILE", "MAGGIO", "GIUGNO",
			"LUGLIO", "AGOSTO", "SETTEMBRE", "OTTOBRE", "NOVEMBRE", "DICEMBRE"};
	
	private JTable tableEntrate; //tabella che mostra la lista delle entrate
	private JTable tableUscite; //tabella che mostra la lista delle uscite
	
	private String colonne_entrate[] = {"Data", "Causale", "Entrata"};
	private String colonne_uscite[] = {"Data", "Causale", "Uscita"};
	private String colonne_calcolo[] = {"Dato calcolato", "Valore"};
	
	private List<Bilancio> lista_entrate = new ArrayList<Bilancio>();
	private List<Bilancio> lista_uscite = new ArrayList<Bilancio>();
	
	String[][] db_entrate;
	String[][] db_uscite;
	Object[][] db_calcolo = {{"Totale entrate: ", new Float(0)},
							 {"Totale uscite: ", new Float(0)},
							 {"Bilancio complessivo: ", new Float(0)}};;
	
							 
	private JTable tableCalcolo;

	public PaginaBilancio() {
		getContentPane().setLayout(null);
		setBounds(200, 200, 1024, 768);

		JLabel lblTitle = new JLabel("BILANCIO");
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 28));
		lblTitle.setBounds(10, 11, 988, 40);
		getContentPane().add(lblTitle);

		JPanel panel_IntervalloDate = new JPanel();
		panel_IntervalloDate.setBounds(653, 60, 376, 339);
		getContentPane().add(panel_IntervalloDate);
		panel_IntervalloDate.setLayout(null);

		JSpinner spinGiornoI = new JSpinner();
		spinGiornoI.setBounds(104, 88, 43, 22);
		panel_IntervalloDate.add(spinGiornoI);
		spinGiornoI.setModel(new SpinnerNumberModel(1, 1, 31, 1));

		JLabel lblGiornoI = new JLabel("Giorno");
		lblGiornoI.setHorizontalAlignment(SwingConstants.CENTER);
		lblGiornoI.setBounds(104, 70, 43, 16);
		panel_IntervalloDate.add(lblGiornoI);


		JSpinner spinMeseI = new JSpinner(new SpinnerListModel(Mesi));
		spinMeseI.setBounds(157, 88, 81, 22);
		panel_IntervalloDate.add(spinMeseI);


		JLabel lblMeseI = new JLabel("Mese");
		lblMeseI.setHorizontalAlignment(SwingConstants.CENTER);
		lblMeseI.setBounds(157, 70, 81, 16);
		panel_IntervalloDate.add(lblMeseI);

		JSpinner spinAnnoI = new JSpinner();
		spinAnnoI.setBounds(248, 88, 63, 22);
		panel_IntervalloDate.add(spinAnnoI);
		spinAnnoI.setModel(new SpinnerNumberModel(2020, 2000, 2050, 1));

		JLabel lblAnnoI = new JLabel("Anno");
		lblAnnoI.setHorizontalAlignment(SwingConstants.CENTER);
		lblAnnoI.setBounds(248, 70, 63, 16);
		panel_IntervalloDate.add(lblAnnoI);

		JSpinner spinGiornoF = new JSpinner(new SpinnerNumberModel(1, 1, 31, 1));
		spinGiornoF.setBounds(104, 162, 43, 22);
		panel_IntervalloDate.add(spinGiornoF);

		JLabel lblGiornoF = new JLabel("Giorno");
		lblGiornoF.setHorizontalAlignment(SwingConstants.CENTER);
		lblGiornoF.setBounds(104, 144, 43, 16);
		panel_IntervalloDate.add(lblGiornoF);

		JSpinner spinMeseF = new JSpinner(new SpinnerListModel(Mesi));
		spinMeseF.setBounds(157, 162, 81, 22);
		panel_IntervalloDate.add(spinMeseF);

		JLabel lblMeseF = new JLabel("Mese");
		lblMeseF.setHorizontalAlignment(SwingConstants.CENTER);
		lblMeseF.setBounds(157, 144, 81, 16);
		panel_IntervalloDate.add(lblMeseF);

		JSpinner spinAnnoF = new JSpinner(new SpinnerNumberModel(2020, 2000, 2050, 1));
		spinAnnoF.setBounds(248, 162, 63, 22);
		panel_IntervalloDate.add(spinAnnoF);

		JLabel lblAnnoF = new JLabel("Anno");
		lblAnnoF.setHorizontalAlignment(SwingConstants.CENTER);
		lblAnnoF.setBounds(248, 144, 63, 16);
		panel_IntervalloDate.add(lblAnnoF);

		JButton btnConferma = new JButton("Conferma");
		btnConferma.setBounds(214, 208, 97, 25);
		panel_IntervalloDate.add(btnConferma);

		JLabel lblDataInizio = new JLabel("Data inizio:");
		lblDataInizio.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataInizio.setBounds(22, 88, 72, 22);
		panel_IntervalloDate.add(lblDataInizio);

		JLabel lblDataFine = new JLabel("Data fine:");
		lblDataFine.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDataFine.setBounds(22, 162, 72, 22);
		panel_IntervalloDate.add(lblDataFine);

		JLabel lblSelezionaIntervalloDate = new JLabel("Seleziona intervallo date");
		lblSelezionaIntervalloDate.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSelezionaIntervalloDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelezionaIntervalloDate.setBounds(22, 11, 289, 25);
		panel_IntervalloDate.add(lblSelezionaIntervalloDate);

		JLabel lblPeriodoSelezionato = new JLabel("Periodo selezionato: nessuno");
		lblPeriodoSelezionato.setHorizontalAlignment(SwingConstants.CENTER);
		lblPeriodoSelezionato.setBounds(22, 246, 304, 16);
		panel_IntervalloDate.add(lblPeriodoSelezionato);

		db_entrate = new String[20][4];
		db_uscite = new String[20][4];
		
		

		JScrollPane scrollPaneEntrate = new JScrollPane();
		scrollPaneEntrate.setBounds(20, 60, 600, 250);
		getContentPane().add(scrollPaneEntrate);
		
		tableEntrate = new JTable();
		scrollPaneEntrate.setViewportView(tableEntrate);
		
		DefaultTableModel modelEntrate = new DefaultTableModel();
        modelEntrate.setColumnIdentifiers(colonne_entrate);
        
		tableEntrate.setModel(modelEntrate);
		tableEntrate.setFillsViewportHeight(true);
		tableEntrate.setColumnSelectionAllowed(true);

		
		
		JScrollPane scrollPaneUscite = new JScrollPane();
		scrollPaneUscite.setBounds(20, 330, 600, 250);
		getContentPane().add(scrollPaneUscite);		
		
		tableUscite = new JTable();
		scrollPaneUscite.setViewportView(tableUscite);
		
		DefaultTableModel modelUscite = new DefaultTableModel();
        modelUscite.setColumnIdentifiers(colonne_uscite);
		
		tableUscite.setModel(modelUscite);
		tableUscite.setFillsViewportHeight(true);
		tableUscite.setColumnSelectionAllowed(true);
		
		
		
		DefaultTableModel modelCalcolo = new DefaultTableModel();
		modelCalcolo.setColumnIdentifiers(colonne_calcolo);
		
		tableCalcolo = new JTable();
		tableCalcolo.setModel(modelCalcolo);
		tableCalcolo.setFillsViewportHeight(true);
		tableCalcolo.setColumnSelectionAllowed(true);
		tableCalcolo.setBounds(22, 623, 598, 60);
		getContentPane().add(tableCalcolo);
		
		JButton btnAnnulla = new JButton("Annulla");
		btnAnnulla.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				SistemaGestionePalestra sistemaGestionePalestra = new SistemaGestionePalestra();
				sistemaGestionePalestra.setVisible(true);
			}
		});
		btnAnnulla.setBounds(912, 644, 86, 39);
		getContentPane().add(btnAnnulla);
		
		modelCalcolo.setDataVector(db_calcolo, colonne_calcolo);
		modelCalcolo.fireTableDataChanged();



		btnConferma.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String gg_I=spinGiornoI.getValue().toString();
				String mm_I=monthToNum(spinMeseI.getValue().toString());
				String aa_I=spinAnnoI.getValue().toString();

				String gg_F=spinGiornoF.getValue().toString();
				String mm_F=monthToNum(spinMeseF.getValue().toString());
				String aa_F=spinAnnoF.getValue().toString();


				lblPeriodoSelezionato.setText("Periodo selezionato: "+ gg_I +"/" +mm_I + "/" + aa_I + " - "+ gg_F +"/" +mm_F + "/" + aa_F);

				String dataInizio = gg_I+"/"+mm_I+"/"+aa_I;
				String dataFine = gg_F+"/"+mm_F+"/"+aa_F;
				

				GestoreBilancio calcoloBilancio = new GestoreBilancio(dataInizio, dataFine);
				
				try {
					Date dataI=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataInizio);
					Date dataF=(Date) (new SimpleDateFormat("dd/MM/yyyy")).parse (dataFine);
					
					if (dataI.compareTo(dataF)>0)
						lblPeriodoSelezionato.setText("PERIODO SELEZIONATO NON VALIDO!");
						
					
					
					
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
				db_calcolo[0][1] = calcoloBilancio.getTotaleEntrate();
				db_calcolo[1][1] = calcoloBilancio.getTotaleUscite();
				db_calcolo[2][1] = calcoloBilancio.getBilancioComplessivo();
				
				modelCalcolo.setDataVector(db_calcolo, colonne_calcolo);
				modelCalcolo.fireTableDataChanged();
								
				
				try {
					lista_entrate = GestoreBilancio.leggiEntrate(dataInizio, dataFine);
					lista_uscite = GestoreBilancio.leggiUscite(dataInizio, dataFine);
				} catch (ParseException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				db_entrate = new String[lista_entrate.size()][4];
				
				for(int i=0; i<lista_entrate.size(); i++)
				{
					db_entrate[i][0] = lista_entrate.get(i).getData();
					db_entrate[i][1] = lista_entrate.get(i).getCausale();
					db_entrate[i][2] = String.valueOf(lista_entrate.get(i).getImporto());
				}
				
				modelEntrate.setDataVector(db_entrate, colonne_entrate);;
				
				modelEntrate.fireTableDataChanged();
				
				db_uscite = new String[lista_uscite.size()][4];
				
				for(int i=0; i<lista_uscite.size(); i++)
				{
					db_uscite[i][0] = lista_uscite.get(i).getData();
					db_uscite[i][1] = lista_uscite.get(i).getCausale();
					db_uscite[i][2] = String.valueOf(lista_uscite.get(i).getImporto());
				}
				
				modelUscite.setDataVector(db_uscite, colonne_uscite);;
				
				modelUscite.fireTableDataChanged();



			}});
	}

	
	/**
	 * Il metodo seguente restituisce il numero del mese posto in ingresso come stringa
	 * @param str_mese
	 * @return
	 */
	private String monthToNum(String str_mese)
	{



		if(str_mese == Mesi[0])
			return "01";
		else if(str_mese == Mesi[1])
			return "02";
		else if(str_mese == Mesi[2])
			return "03";
		else if(str_mese == Mesi[3])
			return "04";
		else if(str_mese == Mesi[4])
			return "05";
		else if(str_mese == Mesi[5])
			return "06";
		else if(str_mese == Mesi[6])
			return "07";
		else if(str_mese == Mesi[7])
			return "08"; 
		else if(str_mese == Mesi[8])
			return "09";
		else if(str_mese == Mesi[9])
			return "10";
		else if(str_mese == Mesi[10])
			return "11";
		else
			return "12";
	}
}